# Symfony Hello world with SQS


# To install

```
composer install
```


# Deploy

Now it is time to deploy.
```
# Create a .env.local.php with dev values
composer dump-env prod
composer install --prefer-dist --optimize-autoloader --no-dev
bin/console cache:warm --env=prod --no-debug

# Create an empty .env.local.php to force using environement variables
echo "<?php return ['APP_ENV'=>'prod'];" > .env.local.php
serverless deploy
rm .env.local.php
```
